;;; ckas-custom-funs.el 

;; Copyright (C) 2021  Christian Kascha

;; Author: Christian Kascha <christian.kascha@posteo.de>
;; Keywords: elisp


    (defun the-the ()
      "Search forward for for a duplicated word."
      (interactive)
      (message "Searching for for duplicated words ...")
      (push-mark)

      ;; This regexp is not perfect but is fairly good over all:
      (if (re-search-forward
	   "\\b\\([^@ \n\t]+\\)[ \n\t]+\\1\\b" nil 'move)
	  (message "Found duplicated word.")
	(message "End of buffer")))

    (defun cka-print-list (aList)
      "Print ALIST at the top of the current buffer."
      (save-excursion
	(goto-char (point-min))
	(insert "/*")
	(newline)
	(while aList
	  (insert (car aList))
	  (newline)
	  (setq aList (cdr aList))
	  )
	(insert "*/")))

  (defun cka-find-table-depencies ()
    (interactive)	 
    (save-excursion
      (let (a (tables ()) (keystrings "from\\|join")) 
      	(goto-char (point-min))
	(while (re-search-forward keystrings nil t)
	  (catch 'continue
	    (search-forward-regexp "(\\|[[:alnum:]_]+" nil 'move)
	    (setq a (match-string 0))
	    (if 
		(string= a "(")
		(throw 'continue nil)
	      (push a tables)
	      )
	    ))
	(setq tables  (sort tables #'string<))
	(setq tables (seq-uniq tables))		
	(cka-print-list tables)
	)))


